"""
Main API Module for ITCP
"""
import logging

from fastapi import FastAPI

from app.config import get_settings

global_settings = get_settings()

# setup loggers
logging.config.fileConfig("app/logging.conf", disable_existing_loggers=False)

# get root logger
logger = logging.getLogger(__name__)

app = FastAPI(
    title=global_settings.app_title,
    description=global_settings.app_description,
    version=global_settings.app_version,
)


@app.on_event("startup")
async def startup_db_client():
    """
    Initialize the Mongo Database at startup
    """
    logger.info("Starting the application...")


@app.on_event("shutdown")
async def shutdown_db_client():
    """
    Close the mongo connection on application shutdown
    """
    logger.info("Shutting down the application...")


@app.get("/health")
async def health_check():
    """
    API Health Check

    Returns:
        str: Success message
    """
    return "success"

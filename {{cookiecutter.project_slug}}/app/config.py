"""
Config
"""
import os
import logging
from functools import lru_cache
from pydantic import BaseSettings

from dotenv import load_dotenv

load_dotenv()
logger = logging.getLogger(__name__)


class Settings(BaseSettings):
    """
    BaseSettings, from Pydantic, validates the data so that when we create an instance of Settings,
    environment and testing will have types of str and bool, respectively.
    Parameters:
    Returns:
    instance of Settings
    """

    environment: str = os.getenv("ENVIRONMENT", "local")

    app_title = os.getenv("APP_TITLE", "APP")
    app_description = os.getenv("APP_DESC", "APP DESC")
    app_version = os.getenv("APP_VERSION", "0.0.1")

    db_url = os.getenv("SQLALCHEMY_URI", "")

    class Config:
        """Config for Base Settings"""

        orm_mode = True


@lru_cache
def get_settings():
    """
    Returns:
        BaseSettings: settings from environment variables
    """
    logger.info("Loading config settings from the environment...")
    return Settings()

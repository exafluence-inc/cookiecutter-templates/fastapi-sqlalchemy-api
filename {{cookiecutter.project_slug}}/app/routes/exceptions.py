"""
Exceptions Helper classes
"""
from fastapi import HTTPException, status


class NotFoundHTTPException(HTTPException):
    """
    Raising a Not Found HTTP Exception
    Args:
        HTTPException (_type_): _description_
    """

    def __init__(self, msg: str):
        super().__init__(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=msg if msg else "Requested resource is not found",
        )
